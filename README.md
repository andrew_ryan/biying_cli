# biying_cli

[![Crates.io](https://img.shields.io/crates/v/biying_cli.svg)](https://crates.io/crates/biying_cli)
[![Rust](https://img.shields.io/badge/rust-1.56.1%2B-blue.svg?maxAge=3600)](https://gitlab.com/andrew_ryan/biying_cli)
[![license](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/andrew_ryan/biying_cli/-/raw/master/LICENSE)

cli for search in biying
```
Usage:
biying_cli <params>
biying_cli rust filetype:pdf
biying_cli javascript
```