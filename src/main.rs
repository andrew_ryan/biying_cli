use doe::args;
use soup::prelude::*;
fn main() {
    if args!().join("") == "help" || args!().join("") == "-h"|| args!().len()==0 {
        println!("Usage:");
        println!("biying_cli <params>");
        println!("biying_cli rust filetype:pdf");
        println!("biying_cli javascript");
    }else{
    let args = args!().join("+");
    let args1 = args.clone();
    let args2 = args.clone();
    let t1 = std::thread::spawn(move || {
        let body = reqwest::blocking::get(&"https://www.bing.com/search?q=dart&FPIG=10C53095BEB349139F8B761878B1477A&first=1&FORM=PERE&setlang=en&ensearch=1".replace("dart", &args1)).unwrap()
    .text().unwrap();
        body
    });
    let t2 = std::thread::spawn(move || {
        let body = reqwest::blocking::get("https://www.bing.com/search?q=dart&FPIG=10C53095BEB349139F8B761878B1477A&first=25&FORM=PERE&setlang=en&ensearch=1".replace("dart", &args2)).unwrap()
        .text().unwrap();
        body
    });
    parse_html(&t1.join().unwrap());
    parse_html(&t2.join().unwrap());
    }
}
fn parse_html(html: &str) {
    let soup = Soup::new(html);
    let ol_opt = soup.tag("ol").find();
    let ol = ol_opt.expect("No results returned!");
    let li = ol.tag("li").find_all();
    let mut tmp_s_v: Vec<String> = vec![];
    li.for_each(|l| {
        let children = l.children();
        for (_, child) in children.enumerate() {
            let h2 = child.tag("h2").find_all();
            for (_, h) in h2.enumerate() {
                let ha = h.tag("a").find();
                match ha {
                    Some(ha) => {
                        let href = ha.get("href");
                        match href {
                            Some(href) => {
                                if href.get(0..4) != Some(&"http".to_string()) {
                                    continue;
                                }
                                if href.get(0..28)
                                    == Some(&"https://www.bing.com/aclick?".to_string())
                                {
                                    continue;
                                }

                                let mut tmp_s_h = format!("{} - {}\n", href, h.text());
                                let div_out = child.tag("div").find_all();
                                let mut tmp_s_p: String = String::from("");
                                let mut last_p: String = String::from("");
                                for (_, div) in div_out.enumerate() {
                                    let p = div.tag("p").find();
                                    match p {
                                        Some(p) => {
                                            let p_t = p.text();
                                            if last_p == p_t {
                                                continue;
                                            }
                                            last_p = p.text();
                                            tmp_s_p = format!("{}\n{}\n", tmp_s_p, p_t)
                                        }
                                        _ => {}
                                    }
                                }
                                tmp_s_h = format!("{}{}", tmp_s_h, tmp_s_p);
                                let mut tmp: Vec<String> = vec![tmp_s_h];
                                tmp_s_v.append(&mut tmp);
                            }
                            _ => {}
                        }
                    }
                    _ => {}
                }
            }
        }
    });
    tmp_s_v.dedup();
    for s in tmp_s_v.iter() {
        print!("{}", s);
    }
}
